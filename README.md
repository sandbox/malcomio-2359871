This module provides an easy way for developers to add accessible tooltips to a site, using jQuery UI Tooltip.

<?php
$tooltip_link = theme('tooltip_link', array(
  'link_text' => t('A tooltip will be displayed when hovering or focusing on this element'),
  'tip_text' => t('This text will appear in the tooltip'),
  'classes' => array(
    'my-class',
    'my-other-class',
  ),
));
?>

The output of this will be:
<code>
<a class="my-class my-other-class tip-trigger" href="javascript:void(0)">A tooltip will be displayed when hovering or focusing on this element<span class="tip-text element-invisible">This text will appear in the tooltip</span></a>
</code>
