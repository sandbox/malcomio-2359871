(function ($) {

  // Add tooltips.
  Drupal.behaviors.toolTip = {
    attach: function (context, settings) {

      $('.tip-trigger').tooltip({
        position: {
          my: "left-60 bottom-15",
          at: "center top"
        },
        items: 'span',
        content: function () {
          return $(this).find('span').text();
        },
        show: false,
        hide: false,
        open: function (event, ui) {
          var $trigger = $(this);
          var $offset = $trigger.offset();
          var scrollTop = $(window).scrollTop();
          var $tooltip = $('.ui-tooltip');

          // If the tooltip is placed to the left of the trigger element, apply
          // the right hand icon to the tooltip.
          // 53px is the default offset of the jquery tooltip when positioned
          // to the right of the trigger.
          if (($offset.left - $tooltip.offset().left) >= 53) {
            $tooltip.addClass('tooltip-arrow-right');
          }

          // If we're close to the top edge, add a class for arrow style.
          // 63px appears to be the jquery ui tooltip buffer for space between
          // the top of the viewport and the tooltip before it repositions to
          // below the element.
          if (($offset.top - ($tooltip.height() + scrollTop) <= 63)) {
            $tooltip.addClass('tooltip-arrow-top');
          }
        }
      });
    }
  }

}(jQuery));